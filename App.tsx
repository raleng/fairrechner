import {useImmer} from 'use-immer';
import {
  Alert,
  NativeModules,
  ScrollView,
  StatusBar,
  Text,
  useColorScheme,
  View,
} from 'react-native';
import {Splits, Contribution} from './components/Splits';
import {colorSchemes} from './styles/colors';
import InfoModal from './components/InfoModal';
import Header from './components/Header';
import AmountToSplit from './components/AmountToSplit';
import AsyncStorage from '@react-native-async-storage/async-storage';
import React, {useEffect, useRef, useState} from 'react';
import styles from './styles/styles';
import ThemeContext, {Theme} from './components/ThemeContext';

const STORAGE_KEY = 'app_state';

export type State = {
  modalVisible: boolean;
  percentMode: boolean;
  amount: number;
  contributions: Contribution[];
};

const newContribution = (): Contribution => {
  return {income: 0, percent: 100};
};

const defaultState = {
  modalVisible: false,
  percentMode: false,
  amount: 0,
  contributions: [newContribution(), newContribution()],
} as State;

const initialState = async (): Promise<State> => {
  const timeoutPromise = new Promise<State>(resolve => {
    setTimeout(() => {
      resolve(defaultState);
    }, 500);
  });

  return await Promise.race([getStateFromStorage(), timeoutPromise]);
};

const getStateFromStorage = async (): Promise<State> => {
  try {
    const value = await AsyncStorage.getItem(STORAGE_KEY);
    return value !== null ? (JSON.parse(value) as State) : defaultState;
  } catch (e) {
    console.error('Error reading data from AsyncStorage.');
    return defaultState;
  }
};
const numberOrZero = (value: string): number =>
  isNaN(Number(value)) ? 0 : Number(value);

const storeState = async (state: State) => {
  try {
    await AsyncStorage.setItem(STORAGE_KEY, JSON.stringify(state));
  } catch (e) {
    console.error('Error storing data.', e);
  }
};

const App = () => {
  const locale = NativeModules.I18nManager.localeIdentifier;
  const language = locale.startsWith('de') ? 'german' : 'english';

  const colorScheme = useColorScheme();
  const scheme = colorScheme ? colorScheme : 'light';
  const [theme, setTheme] = useImmer<Theme>({
    styles: styles(scheme),
    colors: colorSchemes[scheme],
  });

  const firstRender = useRef(true);
  const [loading, setLoading] = useState(true);
  const [state, setState] = useImmer<State>(defaultState);

  useEffect(() => {
    initialState().then(res => {
      setState(res);
      setLoading(false);
    });
  }, []);

  useEffect(() => {
    if (firstRender.current) {
      firstRender.current = false;
    } else {
      if (state !== undefined) {
        storeState(state);
      }
    }
  }, [state]);

  useEffect(() => {
    setTheme((draft: Theme) => {
      draft.styles = colorScheme ? styles(colorScheme) : styles('light');
      draft.colors = colorScheme
        ? colorSchemes[colorScheme]
        : colorSchemes['light'];
    });
  }, [colorScheme]);

  const togglePercentMode = () => {
    setState((draft: State) => {
      draft.percentMode = !draft.percentMode;
    });
  };

  const toggleModal = () => {
    setState((draft: State) => {
      draft.modalVisible = !draft.modalVisible;
    });
  };

  const onChangeAmount = (value: string): void => {
    setState((draft: State) => {
      draft.amount = numberOrZero(value);
    });
  };

  const onChangeIncome =
    (idx: number) =>
    (value: string): void => {
      setState((draft: State) => {
        draft.contributions[idx].income = numberOrZero(value);
      });
    };

  const onChangePercent =
    (idx: number) =>
    (value: string): void => {
      setState((draft: State) => {
        draft.contributions[idx].percent = numberOrZero(value);
      });
    };

  const deleteSplit = (idx: number) => {
    setState((draft: State) => {
      draft.contributions.splice(idx, 1);
    });
  };

  const addSplit = () => {
    setState((draft: State) => {
      draft.contributions.push(newContribution());
    });
  };

  const deleteAlert = (id: number) => {
    Alert.alert(
      'Eintrag Löschen',
      'Wollen Sie diesen Eintrag löschen?',
      [
        {text: 'Abbrechen', onPress: () => {}, style: 'cancel'},
        {text: 'Löschen', onPress: () => deleteSplit(id), style: 'default'},
      ],
      {cancelable: true},
    );
  };

  return (
    <View style={{flex: 1, backgroundColor: theme.colors.background}}>
      <StatusBar
        barStyle={scheme === 'dark' ? 'dark-content' : 'light-content'}
      />
      {loading ? (
        ''
      ) : (
        <ThemeContext.Provider value={theme}>
          <Header
            toggleModal={toggleModal}
            percentMode={state.percentMode}
            togglePercentMode={togglePercentMode}
          />
          <View style={{flex: 1}}>
            <InfoModal
              visible={state.modalVisible}
              toggle={toggleModal}
              language={language}
            />
            <ScrollView contentInsetAdjustmentBehavior="automatic">
              <AmountToSplit
                amount={state.amount}
                onChange={onChangeAmount}
                language={language}
              />
              <Splits
                contributions={state.contributions}
                amountToSplit={state.amount}
                percentMode={state.percentMode}
                onChangeIncome={onChangeIncome}
                onChangePercentage={onChangePercent}
                deleteSplit={deleteAlert}
                addSplit={addSplit}
              />
            </ScrollView>
          </View>
        </ThemeContext.Provider>
      )}
    </View>
  );
};

export default App;
