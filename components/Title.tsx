import Language from '../types/Language';
import {Text as RNText, View} from 'react-native';
import {useContext} from 'react';
import ThemeContext from './ThemeContext';

type TitleTag = 'whatIsFair' | 'percentInfo';
type Titles = {[key in TitleTag]: string};

const titles_de: Titles = {
  whatIsFair: 'Was ist fair?',
  percentInfo: 'Prozentualer Beitrag',
};

const titles_en: Titles = {
  whatIsFair: 'What is fair?',
  percentInfo: 'Contribution percentage',
};

const getTitleByLanguage = (language: Language): Titles =>
  language === 'german' ? titles_de : titles_en;

const Title = ({tag, language}: {tag: TitleTag; language: Language}) => {
  const theme = useContext(ThemeContext);
  const titles = getTitleByLanguage(language);
  return (
    <View style={{flexDirection: 'row'}}>
      <RNText style={{...theme.styles.title, color: theme.colors.text}}>
        #{' '}
      </RNText>
      <RNText style={theme.styles.title}>{titles[tag]}</RNText>
    </View>
  );
};

export default Title;
