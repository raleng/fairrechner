import {useContext} from 'react';
import {Modal, View, TouchableOpacity, ScrollView} from 'react-native';
import Language from '../types/Language';
import Text from './Text';
import ThemeContext from './ThemeContext';
import Title from './Title';

const InfoModal = ({
  visible,
  toggle,
  language,
}: {
  visible: boolean;
  toggle: () => void;
  language: Language;
}) => {
  const theme = useContext(ThemeContext);
  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={visible}
      onRequestClose={toggle}>
      <View style={theme.styles.modal}>
        <ScrollView style={{marginBottom: 12}}>
          <Title tag="whatIsFair" language={language} />
          <Text tag="whatIsFair" language={language} />
          <Title tag={'percentInfo'} language={language} />
          <Text tag="percentInfo" language={language} />
        </ScrollView>
        <TouchableOpacity style={theme.styles.closeButton} onPress={toggle}>
          <Text tag={'closeButton'} language={language} />
        </TouchableOpacity>
      </View>
    </Modal>
  );
};

export default InfoModal;
