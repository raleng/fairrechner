import {useContext} from 'react';
import {View, Text} from 'react-native';
import IconButton from './IconButton';
import ThemeContext from './ThemeContext';

const Header = ({
  toggleModal,
  percentMode,
  togglePercentMode,
}: {
  toggleModal: () => void;
  percentMode: boolean;
  togglePercentMode: () => void;
}) => {
  const theme = useContext(ThemeContext);
  return (
    <View style={theme.styles.header}>
      <View style={theme.styles.headerTextContainer}>
        <Text style={theme.styles.headerText}>
          <Text style={theme.styles.headerTextPrimary}>Fair</Text>Rechner
        </Text>
      </View>
      <View style={theme.styles.headerIconContainer}>
        <IconButton
          iconName="Percent"
          onPress={togglePercentMode}
          active={percentMode}
        />
        <IconButton iconName="Info" onPress={toggleModal} />
      </View>
    </View>
  );
};

export default Header;
