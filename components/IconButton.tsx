import {TouchableOpacity} from 'react-native';
import {Percent, Info} from 'phosphor-react-native';
import {useContext} from 'react';
import ThemeContext from './ThemeContext';

const icons = {Percent, Info};
type IconName = keyof typeof icons;

const IconButton = ({
  iconName,
  onPress,
  active = false,
}: {
  iconName: IconName;
  onPress: () => void;
  active?: boolean;
}) => {
  const theme = useContext(ThemeContext);
  const Icon = icons[iconName];
  const bgColor = active
    ? theme.colors.primaryLight
    : theme.colors.buttonBackground;
  const iconColor = active ? theme.colors.activeButtonText : theme.colors.text;
  return (
    <TouchableOpacity
      style={{...theme.styles.iconButton, backgroundColor: bgColor}}
      onPress={onPress}>
      <Icon color={iconColor} />
    </TouchableOpacity>
  );
};

export default IconButton;
