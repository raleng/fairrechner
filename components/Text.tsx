import {useContext} from 'react';
import {Text as RNText, View} from 'react-native';
import Language from '../types/Language';
import ThemeContext from './ThemeContext';

type TextKeys = 'amountToSplit' | 'whatIsFair' | 'percentInfo' | 'closeButton';

type Texts = {[key in TextKeys]: string[]};

const texts_de: Texts = {
  amountToSplit: ['Geldbetrag'],
  whatIsFair: [
    `Kosten in gleichen Teilen unter Personen mit unterschiedlichen finanziellen Möglichkeiten aufzuteilen, ist es unter Ümständen nicht sehr fair.`,
    `Wenn man annimmt, dass alle Beteiligten einen ähnlichen Beitrag leisten und ähnlich viel Freizeit haben, kann es als fairer sein Kosten anteilig zum Einkommen aufzuteilen.`,
  ],
  percentInfo: [
    `Möchte man Unterschiede in Beitrag und Freizeit berücksichtigen, kann man Prozentsätze aktivieren und anpassen. Allerdings ist es ggf. nicht fair wenn der Prozentsatz einfach die Arbeitsstunden abbildet.`,
    `Schichtdienst kann auch bei weniger Stunden anstrengender sein. Jemand arbeitet vielleicht nur halbtags, übernimmt aber mehr Arbeiten im Haushalt oder bildet sich neben der Arbeit weiter.`,
  ],
  closeButton: ['Schließen'],
};

const texts_en: Texts = {
  amountToSplit: ['Amount'],
  whatIsFair: [
    `Splitting costs equally among people with very different financial resources might not be the fairest way.`,
    `Assuming everyone is putting in the same effort and having similar amounts of free time, it might be fairer to split costs proportional to income.`,
  ],
  percentInfo: [
    `If you want to account for different contribution levels, you can activate and adjust percentages to scale the effort everyone puts in. However, simply converting working hours to percentages might not be fair either.`,
    `Shift work can be more exhausting even when working fewer hours. Someone might work part-time but spend extra time on care work or their education.`,
  ],
  closeButton: ['Close'],
};

const getTextsByLanguage = (language: Language): Texts =>
  language === 'german' ? texts_de : texts_en;

const Text = ({tag, language}: {tag: TextKeys; language: Language}) => {
  const theme = useContext(ThemeContext);
  const texts = getTextsByLanguage(language);
  return (
    <View>
      {texts[tag].map((t, idx) => (
        <RNText key={`${tag}${idx}`} style={theme.styles.text}>
          {t}
        </RNText>
      ))}
    </View>
  );
};

export default Text;
