import {View, Text, TextInput, TouchableOpacity} from 'react-native';
import {
  Bank,
  Percent,
  ChartPieSlice,
  Coin,
  UserPlus,
} from 'phosphor-react-native';
import ThemeContext from './ThemeContext';
import {useContext} from 'react';

export type Contribution = {income: number; percent: number};
type Share = {percent: number; amount: number};

const scaledIncome = (
  contribution: Contribution,
  percentMode: boolean,
): number => {
  if (!percentMode) {
    return contribution.income;
  } else if (contribution.percent === 0) {
    return 0;
  } else {
    return contribution.income / (contribution.percent / 100);
  }
};

const share = (
  toSplit: number,
  contribution: Contribution,
  summedIncome: number,
  percentMode: boolean,
): Share => {
  const income = scaledIncome(contribution, percentMode);
  const sharePercent = income / summedIncome;
  const percent = !isNaN(sharePercent) ? Math.round(sharePercent * 100) : 0;
  const amount = !isNaN(sharePercent) ? Math.round(sharePercent * toSplit) : 0;
  return {percent, amount};
};

const AddSplit = ({onClick}: {onClick: () => void}) => {
  const theme = useContext(ThemeContext);
  return (
    <TouchableOpacity onPress={onClick} style={theme.styles.addButton}>
      <UserPlus size={32} color={theme.colors.text} />
    </TouchableOpacity>
  );
};

const IncomeInput = ({
  income,
  onChange,
}: {
  income: number;
  onChange: (value: string) => void;
}) => {
  const theme = useContext(ThemeContext);
  return (
    <View style={{...theme.styles.inputContainer, flex: 3, marginRight: 12}}>
      <Bank style={theme.styles.icon} color={theme.colors.text} />
      <TextInput
        style={{...theme.styles.input, flex: 2}}
        onChangeText={onChange}
        value={income.toString()}
        keyboardType={'numeric'}
        selectTextOnFocus
      />
    </View>
  );
};

const PercentageInput = ({
  percent,
  onChange,
}: {
  percent: number;
  onChange: (value: string) => void;
}) => {
  const theme = useContext(ThemeContext);
  return (
    <View style={{...theme.styles.inputContainer, flex: 2}}>
      <Percent style={theme.styles.icon} color={theme.colors.text} />
      <TextInput
        style={{...theme.styles.input, flex: 1}}
        onChangeText={onChange}
        value={percent.toString()}
        keyboardType={'numeric'}
        selectTextOnFocus
      />
    </View>
  );
};

const SharePercent = ({value}: {value: number}) => {
  const theme = useContext(ThemeContext);
  return (
    <View style={{flex: 4, ...theme.styles.resultView}}>
      <ChartPieSlice style={theme.styles.icon} color={theme.colors.text} />
      <Text style={{...theme.styles.text}}>{`${value}%`}</Text>
    </View>
  );
};

const ShareAmount = ({value}: {value: number}) => {
  const theme = useContext(ThemeContext);
  return (
    <View style={{flex: 5, ...theme.styles.resultView}}>
      <Coin style={theme.styles.icon} color={theme.colors.text} />
      <Text style={{...theme.styles.text}}>{`${value}`}</Text>
    </View>
  );
};

const Split = ({
  idx,
  amountToSplit,
  contribution,
  summedIncome,
  percentMode,
  onChangeIncome,
  onChangePercentage,
  deleteSplit,
}: {
  idx: number;
  amountToSplit: number;
  contribution: Contribution;
  summedIncome: number;
  percentMode: boolean;
  onChangeIncome: (value: string) => void;
  onChangePercentage: (value: string) => void;
  deleteSplit: (id: number) => void;
}) => {
  const {percent, amount} = share(
    amountToSplit,
    contribution,
    summedIncome,
    percentMode,
  );
  const theme = useContext(ThemeContext);
  return (
    <TouchableOpacity
      onLongPress={() => deleteSplit(idx)}
      style={theme.styles.container}>
      <View style={{flexDirection: 'row'}}>
        <View style={{flexDirection: 'row', flex: 1, paddingLeft: 12}}>
          <IncomeInput income={contribution.income} onChange={onChangeIncome} />
          {percentMode ? (
            <PercentageInput
              percent={contribution.percent}
              onChange={onChangePercentage}
            />
          ) : (
            ''
          )}
        </View>
        <View style={{flexDirection: 'row', flex: 1, marginLeft: 12}}>
          <SharePercent value={percent} />
          <ShareAmount value={amount} />
        </View>
      </View>
    </TouchableOpacity>
  );
};

export const Splits = ({
  contributions,
  amountToSplit,
  percentMode,
  onChangeIncome,
  onChangePercentage,
  deleteSplit,
  addSplit,
}: {
  contributions: Contribution[];
  amountToSplit: number;
  percentMode: boolean;
  onChangeIncome: (idx: number) => (value: string) => void;
  onChangePercentage: (idx: number) => (value: string) => void;
  deleteSplit: (idx: number) => void;
  addSplit: () => void;
}) => {
  const summedIncome = contributions
    .map(c => scaledIncome(c, percentMode))
    .reduce((a, b) => a + b, 0);
  return (
    <View style={{marginTop: 24}}>
      {contributions.map((c, idx) => (
        <Split
          key={idx}
          idx={idx}
          amountToSplit={amountToSplit}
          summedIncome={summedIncome}
          contribution={c}
          percentMode={percentMode}
          onChangeIncome={onChangeIncome(idx)}
          onChangePercentage={onChangePercentage(idx)}
          deleteSplit={deleteSplit}
        />
      ))}
      <AddSplit onClick={addSplit} />
    </View>
  );
};
