import {View, TextInput} from 'react-native';
import {Coins} from 'phosphor-react-native';
import Text from './Text';
import Language from '../types/Language';
import ThemeContext from './ThemeContext';
import {useContext} from 'react';

const AmountToSplit = ({
  amount,
  onChange,
  language,
}: {
  amount: number;
  onChange: (value: string) => void;
  language: Language;
}) => {
  const theme = useContext(ThemeContext);
  return (
    <View
      style={{
        ...theme.styles.container,
        paddingTop: 16,
        paddingBottom: 16,
        paddingLeft: 16,
        paddingRight: 16,
        marginTop: 12,
      }}>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-evenly',
        }}>
        <View
          style={{
            width: '33%',
            paddingLeft: 8,
            paddingBottom: 4,
            paddingTop: 4,
          }}>
          <Text tag={'amountToSplit'} language={language} />
        </View>
        <View
          style={{flexDirection: 'row', width: '33%', alignItems: 'center'}}>
          <Coins style={theme.styles.icon} color={theme.colors.text} />
          <TextInput
            style={{...theme.styles.input, flex: 1}}
            onChangeText={onChange}
            value={amount.toString()}
            keyboardType={'numeric'}
            selectTextOnFocus
          />
        </View>
      </View>
    </View>
  );
};

export default AmountToSplit;
