import React from 'react';
import colorSchemes, {Colors} from '../styles/colors';
import styles, {Styles} from '../styles/styles';

export type Theme = {styles: Styles; colors: Colors};
const defaultTheme = {
  styles: styles('light'),
  colors: colorSchemes['light'],
} as Theme;
const ThemeContext = React.createContext(defaultTheme);

export default ThemeContext;
