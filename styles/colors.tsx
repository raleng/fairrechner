const light = {
  primary: '#059669',
  primaryLight: '#a7f3d0',
  text: 'hsla(0, 0%, 30%, 1)',
  activeButtonText: 'hsla(0, 0%, 30%, 1)',
  background: 'hsla(0, 0%, 97%, 1)',
  buttonBackground: 'hsla(0, 0%, 90%, 1)',
  containerBackground: 'hsla(0, 0%, 100%, 1)',
  inputBackground: 'hsla(0, 0%, 96%, 1)',
};

const dark = {
  primary: '#059669',
  primaryLight: '#a7f3d0',
  text: 'hsla(0, 0%, 70%, 1)',
  activeButtonText: 'hsla(0, 0%, 30%, 1)',
  background: 'hsla(0, 0%, 10%, 1)',
  buttonBackground: 'hsla(0, 0%, 32%, 1)',
  containerBackground: 'hsla(0, 0%, 20%, 1)',
  inputBackground: 'hsla(0, 0%, 25%, 1)',
} as Colors;

type ColorMode = 'light' | 'dark';
export type Colors = {
  primary: string;
  primaryLight: string;
  text: string;
  activeButtonText: string;
  background: string;
  buttonBackground: string;
  containerBackground: string;
  inputBackground: string;
};
type ColorsMap = {[key in ColorMode]: Colors};

export const colorSchemes: ColorsMap = {
  light: light,
  dark: dark,
};

export default colorSchemes;
