import {StyleSheet} from 'react-native';
import {colorSchemes} from './colors';

const styles = (theme: 'light' | 'dark') => {
  const colors = colorSchemes[theme];
  return StyleSheet.create({
    header: {
      padding: 12,
      marginTop: 8,
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
    },
    headerText: {
      color: colors.text,
      fontSize: 20,
      fontFamily: 'iosevka-aile-extrabold',
    },
    headerTextPrimary: {
      color: colors.primary,
    },
    headerTextContainer: {flex: 3},
    headerIconContainer: {
      flexDirection: 'row',
      flex: 1,
      justifyContent: 'space-between',
    },
    title: {
      color: colors.primary,
      fontSize: 20,
      fontFamily: 'iosevka-aile-extrabold',
      marginTop: 8,
      marginBottom: 8,
    },
    text: {
      color: colors.text,
      fontSize: 16,
      fontFamily: 'iosevka-aile-regular',
      lineHeight: 20,
      marginTop: 4,
      marginBottom: 4,
    },
    container: {
      backgroundColor: colors.containerBackground,
      paddingTop: 12,
      paddingBottom: 12,
      elevation: 2,
      flex: 1,
      borderWidth: 0.1,
    },
    input: {
      color: colors.text,
      backgroundColor: colors.inputBackground,
      borderRadius: 4,
      fontWeight: 'bold',
      fontFamily: 'iosevka-aile-regular',
      textAlign: 'right',
      padding: 4,
    },
    inputContainer: {
      flexDirection: 'row',
      alignItems: 'center',
    },
    icon: {
      justifyContent: 'center',
      marginLeft: 4,
      marginRight: 4,
    },
    resultView: {
      flexDirection: 'row',
      alignItems: 'center',
      paddingLeft: 4,
      paddingTop: 16,
      paddingBottom: 16,
    },
    addButton: {
      backgroundColor: colors.buttonBackground,
      justifyContent: 'center',
      alignItems: 'center',
      textAlignVertical: 'center',
      textAlign: 'center',
      height: 60,
      elevation: 2,
    },
    closeButton: {
      backgroundColor: colors.buttonBackground,
      justifyContent: 'center',
      alignItems: 'center',
      textAlignVertical: 'center',
      textAlign: 'center',
      height: 50,
      elevation: 2,
    },
    iconButton: {
      alignItems: 'center',
      justifyContent: 'center',
      borderRadius: 50,
      padding: 8,
    },
    modal: {
      backgroundColor: colors.background,
      maxHeight: '95%',
      padding: 12,
      margin: 24,
      elevation: 20,
    },
  });
};

const _styles = styles('light');
export type Styles = typeof _styles;

export default styles;
