# FairRechner Privacy Policy

I hereby state, to the best of my knowledge and belief, that I have not programmed this app to collect any personally identifiable information. All data (app preferences (like theme, etc.) and alarms) created by the you (the user) is stored on your device only, and can be simply erased by clearing the app's data or uninstalling it.

This app does not require any permissions and will function fully offline. You can check the `AndroidManifest.xml` in the repository to see that no permissions are requested.

If you have any concerns or questions about how this app access and handles your data, please contact me via `ralengdev@gmail.com`.

Sincerely,
Ralf Engbers
